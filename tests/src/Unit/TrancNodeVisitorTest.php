<?php

namespace Drupal\Tests\tranc\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Template\TwigExtension;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\tranc\TrancTwigExtension;
use Twig\Environment;
use Twig\Loader\ArrayLoader;
use Twig\Source;

/**
 * Tests the TrancNodeVisitor.
 */
class TrancNodeVisitorTest extends UnitTestCase {
  
  use ProphecyTrait;

  /**
   * The tranc twig extension.
   *
   * @var \Drupal\tranc\TrancTwigExtension
   */
  protected $trancTwigExtension;

  /**
   * The core twig extension.
   *
   * @var \Drupal\Core\Template\TwigExtension
   */
  protected $coreTwigExtension;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $themeHandler = $this->prophesize(ThemeHandlerInterface::class);
    $themeHandler->getDefault()->willReturn('default');
    $themeHandler->getTheme('default')->willReturn(new Extension('', '', ''));
    $this->trancTwigExtension = new TrancTwigExtension($themeHandler->reveal());
    $renderer = $this->prophesize(RendererInterface::class)->reveal();
    $urlGenerator = $this->prophesize(UrlGeneratorInterface::class)->reveal();
    $themeManager = $this->prophesize(ThemeManagerInterface::class)->reveal();
    $dateFormatter = $this->prophesize(DateFormatterInterface::class)->reveal();
    $this->coreTwigExtension = new TwigExtension($renderer, $urlGenerator, $themeManager, $dateFormatter);
  }

  /**
   * Tests the tranc node visitor.
   *
   * @param string $template
   *   A twig template.
   * @param string $expected
   *   The compiled template is expected to contain this string.
   * @param bool $fire
   *   Whether the TrancNodeVisitor should fire.
   *
   * @dataProvider providerTranc
   */
  public function testTrancNodeVisitor($template, $expected, $fire = TRUE) {
    // realpath() matches the new Extension('', '', '') in ::setUp() and this
    // makes TrancNodeVisitor fire always. On the other hand, realpath() will
    // never start with two slashes.
    $templateName = $fire ? realpath('') : '//';
    $loader = new ArrayLoader([$templateName => '']);
    $twig = new Environment($loader);
    $twig->addExtension($this->coreTwigExtension);
    $twig->addExtension($this->trancTwigExtension);
    $stream = $twig->tokenize(new Source($template, $templateName));
    $nodes = $twig->parse($stream);
    $this->assertStringContainsString($expected, $twig->compile($nodes));
  }

  /**
   * Each test is twig template, an expected partial outcome.
   */
  public function providerTranc() {
    return [
      ['{% trans %}x{% endtrans %}', '"langcode" => "[content]"'],
      ["{% trans with {'context': 'foo'} %}x{% endtrans %}", '"langcode" => "[content]"'],
      ["{% trans with {'context': 'foo', 'langcode': 'bar'} %}x{% endtrans %}", '"langcode" => "bar"'],
      ['{{ foo|t }}', 'renderVar(tc('],
      ['{{ foo|t }}', 'renderVar(t(', FALSE],
    ];
  }

}
