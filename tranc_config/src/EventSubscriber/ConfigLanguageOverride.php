<?php

namespace Drupal\tranc_config\EventSubscriber;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\AdminContext;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Set the appropriate config override language.
 *
 * The core LanguageRequestSubscriber sets the interface language as the
 * config override language. This class sets it to content if this is not an
 * admin route. This can not be achieved by simply patching
 * LanguageRequestSubscriber because it runs before routing. This class runs
 * after routing.
 */
class ConfigLanguageOverride implements EventSubscriberInterface {

  public function __construct(protected LanguageManagerInterface $languageManager, protected AdminContext $adminContext) {

  }

  public function afterRoutingConfigLanguageOverride(RequestEvent $event) {
    if ($event->isMainRequest() && !$this->adminContext->isAdminRoute()) {
      $this->languageManager->setConfigOverrideLanguage($this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Symfony\Component\HttpKernel\EventListener\RouterListener is 32.
    $events[KernelEvents::REQUEST][] = ['afterRoutingConfigLanguageOverride', 31];
    return $events;
  }

}
