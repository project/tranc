This module changes the `t` and `trans` filters and the
[`% trans %`](https://www.drupal.org/node/2047135)
tag in the default theme to use the content language as default instead of
the interface language.

Also, new filters have been added: `tc` / `transc` translates to the content
language in any theme, `ti` / `transi` translates to the interface language.

Currently there is no way to force the `% trans %` tag to translate to the
interface language as it is unlikely the shorter strings of interface would
require this tag. The `[content]` langcode can be used to translate to the
content language in any theme:

    {% trans with {'langcode': '[content]'} %}

For module developers, the `[content]` langcode can also be used in `t()`,
`new TranslatableMarkup()`, `StringTranslationTrait::t()` which means every
`$this->t()` call and also in any method calls to the
`string_translation` service. The `TrancTranslationManager::CONTENT_LANGCODE`
constant can also be used.

For even shorter code, a new function called `tc` is also introduced, it is
the same as `t`, except defaults to the content language instead of the
interface language.
