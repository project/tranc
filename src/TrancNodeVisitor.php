<?php

namespace Drupal\tranc;

use Drupal\Core\Template\TwigNodeTrans;
use Twig\Environment;
use Twig\Node\Expression\ArrayExpression;
use Twig\Node\Expression\ConstantExpression;
use Twig\Node\Expression\FilterExpression;
use Twig\Node\Node;
use Twig\NodeVisitor\NodeVisitorInterface;

/**
 * Changes the t and trans filters to tc in the default theme.
 */
class TrancNodeVisitor implements NodeVisitorInterface {

  /**
   * TrancNodeVisitor constructor.
   *
   * @param string $path
   *   The path to the default theme.
   */
  public function __construct(protected string $path) {
  }


  /**
   * {@inheritdoc}
   */
  public function enterNode(Node $node, Environment $env): Node {
    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function leaveNode(Node $node, Environment $env): ?Node {
    if (str_starts_with(realpath((string) $node->getTemplateName()), $this->path)) {
      if ($node instanceof FilterExpression) {
        $name = $node->getNode('filter')->getAttribute('value');
        if ($name === 't' || $name === 'trans') {
          // Use our own translation filter.
          $node->getNode('filter')->setAttribute('value', 'tc');
        }
      }
      elseif ($node instanceof TwigNodeTrans) {
        $lineno = $node->getTemplateLine();
        if ($node->hasNode('options')) {
          $options = $node->getNode('options');
        }
        else {
          $options = new ArrayExpression([], $lineno);
          $node->setNode('options', $options);
        }
        $langcodeExpression = new ConstantExpression('langcode', $lineno);
        if (!$options->hasElement($langcodeExpression)) {
          $contentExpression = new ConstantExpression(TrancTranslationManager::CONTENT_LANGCODE, $lineno);
          $options->addElement($contentExpression, $langcodeExpression);
        }
      }
    }
    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriority() {
    // Copied from Drupal\Core\Template\TwigNodeVisitor.
    return 256;
  }

}
