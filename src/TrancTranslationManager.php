<?php

namespace Drupal\tranc;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;

/**
 * Replaces the [content] langcode with the current content langcode.
 */
class TrancTranslationManager extends TranslationManager {

  /**
   * Use this langcode to translate to content language.
   */
  const CONTENT_LANGCODE = '[content]';

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The translation manager.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $translationManager;

  /**
   * TrancTranslationManager constructor.
   *
   * @param \Drupal\Core\StringTranslation\TranslationManager $translationManager
   *   The translation manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(TranslationManager $translationManager, LanguageManagerInterface $languageManager) {
    $this->translationManager = $translationManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context) {
    return $this->translationManager->getStringTranslation($this->replaceLangcode($langcode), $string, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function translate($string, array $args = [], array $options = []) {
    return $this->translationManager->translate($string, $args, $this->replaceLangcodeInOptions($options));
  }

  public function addTranslator(TranslatorInterface $translator, $priority = 0) {
    $this->translationManager->addTranslator($translator, $priority);
    return $this;
  }

  protected function sortTranslators() {
    return $this->translationManager->sortTranslators();
  }

  protected function doTranslate($string, array $options = []) {
    return $this->translationManager->doTranslate($string, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function translateString(TranslatableMarkup $translated_string) {
    return $this->translationManager->translateString(new TranslatableMarkup(
      // @codingStandardsIgnoreLine
      $translated_string->getUntranslatedString(),
      $translated_string->getArguments(),
      $this->replaceLangcodeInOptions($translated_string->getOptions())
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function formatPlural($count, $singular, $plural, array $args = [], array $options = []) {
    return $this->translationManager->formatPlural($count, $singular, $plural, $args, $this->replaceLangcodeInOptions($options));
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->translationManager->reset();
  }

  /**
   * Sets the default langcode.
   *
   * This is not a method on either interface but LanguageRequestSubscriber
   * calls it, so ¯\_(ツ)_/¯.
   *
   * @param string $langcode
   *   A language code.
   */
  public function setDefaultLangcode($langcode) {
    return $this->translationManager->setDefaultLangcode($langcode);
  }

  /**
   * Replace %content% with the current content language.
   *
   * @param array $options
   *   Options as passed to TranslationInterface::translate().
   *
   * @return array
   *   Options with langcode replaced.
   */
  protected function replaceLangcodeInOptions(array $options) {
    if (isset($options['langcode'])) {
      $options['langcode'] = $this->replaceLangcode($options['langcode']);
    }
    return $options;
  }

  /**
   * Replaces the [content] langcode with the current content langcode.
   *
   * @param string $langcode
   *   The langcode.
   *
   * @return string
   *   If langcode was [content] it is changed to the current content langcode.
   */
  protected function replaceLangcode(string $langcode) {
    if ($langcode === self::CONTENT_LANGCODE) {
      // It is up to the language manager to make this call cheap. Storing this
      // string in a property could introduce a problem if the current content
      // language changed mid-request.
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }
    return $langcode;
  }

}
