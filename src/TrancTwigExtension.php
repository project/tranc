<?php

namespace Drupal\tranc;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Wires in the TrancNodeVisitor.
 */
class TrancTwigExtension extends AbstractExtension {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * TrancTwigExtension constructor.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   */
  public function __construct(ThemeHandlerInterface $themeHandler) {
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeVisitors() {
    $default = $this->themeHandler->getTheme($this->themeHandler->getDefault());
    return [new TrancNodeVisitor(realpath($default->getPath()))];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('tc', 'tc', ['is_safe' => ['html']]),
      new TwigFilter('transc', 'tc', ['is_safe' => ['html']]),
      new TwigFilter('ti', 't', ['is_safe' => ['html']]),
      new TwigFilter('transi', 't', ['is_safe' => ['html']]),
    ];
  }

}
